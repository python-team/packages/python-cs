Source: python-cs
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Vincent Bernat <bernat@debian.org>
Build-Depends: debhelper-compat (= 13), dh-sequence-python3,
               python3-all,
               python3-setuptools,
               python3-requests,
               python3-tz,
               python3-pytest,
               python3-pytest-runner,
               python3-pytest-cov,
               python3-aiohttp
Standards-Version: 4.6.2
Homepage: https://github.com/exoscale/cs
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/python-team/packages/python-cs.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-cs

Package: cs
Architecture: all
Section: utils
Depends: ${python3:Depends}, ${misc:Depends},
         python3-cs (= ${binary:Version})
Suggests: python3-pygments
Description: simple, yet powerful CloudStack API client
 cs a simple, yet powerful Apache CloudStack API client written in
 Python and available as a command-line tool. It is a thin wrapper on
 top of the CloudStack API and hence it is able to adapt to any future
 version.
 .
 Apache CloudStack is open source software designed to deploy and
 manage large networks of virtual machines.

Package: python3-cs
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Description: simple, yet powerful CloudStack API client (Python 3 module)
 This module is a simple, yet powerful Apache CloudStack API client
 for Python. It is a thin wrapper on top of the CloudStack API and
 hence it is able to adapt to any future version.
 .
 Apache CloudStack is open source software designed to deploy and
 manage large networks of virtual machines.
 .
 This package contains the Python 3 module.
